module Main where

import Data.Text.Lazy.Encoding (encodeUtf8)
import Data.ByteString.Lazy.Char8 qualified as BS

import Options qualified
import Feed qualified

main :: IO ()
main = Options.parse >>= \opts -> do
  feed <- Feed.fetch opts
  BS.writeFile (Options.fileName opts) $
    encodeUtf8 (Feed.render feed)
