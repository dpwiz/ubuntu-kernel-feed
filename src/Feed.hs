module Feed where

import Data.List (sortOn)
import Data.Maybe (fromJust)
import Data.Ord (Down(..))
import Data.Text (Text)
import Data.Text qualified as Text
import Data.Text.Lazy qualified as Lazy
import Data.Time (getCurrentTime)
import Text.Atom.Feed qualified as Atom
import Text.Feed.Export qualified as Export
import Text.Feed.Types qualified as Feed
import Text.XML qualified as XML

import Kernel (Kernel)
import Kernel qualified
import Options (Options)

render :: Atom.Feed -> Lazy.Text
render feed =
  fromJust $ Export.textFeedWith options $
    Feed.AtomFeed feed
  where
    options = XML.def
      { XML.rsPretty = True
      }

fetch :: Options -> IO Atom.Feed
fetch opts = do
  now <- getCurrentTime
  let
    emptyFeed = Atom.nullFeed
       (Text.pack Kernel.PPA_URL)
       (Atom.TextString "")
       (Text.pack $ show now)

  kernels <- Kernel.listKernels opts
  pure emptyFeed
    { Atom.feedEntries =
        map toEntry $
          sortOn (Down . Kernel.time) kernels
    }

toEntry :: Kernel -> Atom.Entry
toEntry kernel = emptyEntry
  { Atom.entryContent =
      fmap Atom.HTMLContent $
        Kernel.header kernel <>
        fmap changesLink (Kernel.changes kernel)
  }
  where
    emptyEntry = Atom.nullEntry
      (Text.pack $ Kernel.PPA_URL <> show (Kernel.version kernel))
      (Atom.TextString . Text.pack $ show $ Kernel.version kernel)
      (Text.pack $ show $ Kernel.time kernel)

changesLink :: Text -> Text
changesLink url =
  "<a href=\"" <> url <> "\">CHANGES</a>\n<br>"
