module Version
  ( Version(..)
  , parse
  , render
  , isRC
  ) where

import Data.String (IsString (fromString))
import Data.Text (Text)
import Data.Text qualified as Text
import Text.Read (readMaybe)

isRC :: Version -> Bool
isRC = \case
  Version{suffix=[]} ->
    False
  _rc_somwhere_in_there ->
    True

data Version = Version
  { major :: Major
  , minor :: Maybe Int
  , suffix :: [Text]
  }
  deriving (Eq)

instance Ord Version where
  compare a b =
    compare (major a) (major b) <>
    compare (minor a) (minor b) <>
    compareSuffix (suffix a) (suffix b)

-- instance IsString Version where
--   fromString = fromJust . parse . Text.pack

instance Show Version where
  show = render

render :: IsString a => Version -> a
render Version{..} = fromString $ mconcat
    [ "v"
    , show major
    , foldMap (('.':) . show) minor
    , foldMap (('-':) . Text.unpack) suffix
    ]

parse :: Text -> Maybe Version
parse =
  parsePieces .
  Text.splitOn "." .
  Text.dropWhile (== 'v')

parsePieces :: [Text] -> Maybe Version
parsePieces = \case
  [x, yy] -> do
    let (version, suffix) = parseSuffix yy
    major <- Major <$> readText x <*> version
    let minor = Nothing
    pure Version{..}

  [x, yy, zzz] -> do
    major <- Major <$> readText x <*> readText yy
    let (minor, suffix) = parseSuffix zzz
    pure Version{..}

  _ ->
    Nothing
  where
    readText = readMaybe @Int . Text.unpack

    parseSuffix str =  (readText num, suff)
      where
        num : suff = Text.splitOn "-" str

-- "4.20.minor"
data Major = Major Int Int
  deriving (Eq, Ord)

instance Show Major where
  show (Major x yy) = show x <> "." <> show yy

compareSuffix :: Ord a => [a] -> [a] -> Ordering
compareSuffix [] [] = EQ
compareSuffix [] _ = GT
compareSuffix _ [] = LT
compareSuffix xs ys = compare xs ys
