module Kernel
  ( Kernel(..)
  , listKernels
  , pattern PPA_URL
  ) where

import Data.ByteString (ByteString)
import Data.ByteString qualified as ByteString
import Data.Foldable (for_)
import Data.List (sort)
import Data.Maybe (mapMaybe)
import Data.String (IsString(..))
import Data.Text (Text)
import Data.Text qualified as Text
import Data.Text.Encoding  (decodeUtf8, encodeUtf8)
import Data.Time (UTCTime, getCurrentTime)
import Debug.Trace  (traceM)
import Network.HTTP.Directory (httpDirectory', httpLastModified', noTrailingSlash, httpExists')
import Network.HTTP.Simple (parseRequest_, getResponseBody, httpBS)
import System.Directory (createDirectoryIfMissing, doesFileExist)
import System.FilePath ((</>))

import Version (Version)
import Version qualified
import Options (Options)
import Options qualified

data Kernel = Kernel
  { version :: Version
  , header :: Maybe Text
  , changes :: Maybe Text
  , time :: UTCTime
  }

pattern PPA_URL :: (IsString a, Eq a) => a
pattern PPA_URL = "https://kernel.ubuntu.com/~kernel-ppa/mainline/"

listKernels :: Options -> IO [Kernel]
listKernels options = do
  fetchIndex options >>= traverse (fetchDetails options)

fetchIndex :: Options ->  IO [Version]
fetchIndex options = do
  createDirectoryIfMissing True (Options.cacheDir options)
  traceM "Fetching index"
  stuff <- httpDirectory' PPA_URL
  pure $
    sort . filter required $
      mapMaybe (Version.parse . noTrailingSlash) stuff
  where
    required v =
      v >= Options.minVersion options &&
      not (Options.noRC options && Version.isRC v)

fetchDetails :: Options -> Version -> IO Kernel
fetchDetails options version = do
  traceM $ "Fetching details for " <> show version
  createDirectoryIfMissing True detailsCacheDir
  header <- fetchHeader detailsCacheDir version
  changes <- fetchChangelog detailsCacheDir version
  time <- fetchLastModified detailsCacheDir version
  pure Kernel{..}
  where
    detailsCacheDir = Options.cacheDir options </> Version.render version

fetchHeader :: FilePath -> Version -> IO (Maybe Text)
fetchHeader cacheDir ver = do
  bytes <- fetch cacheFile url
  pure $ fmap decodeUtf8 bytes
  where
    cacheFile = cacheDir </> "HEADER.html"
    url = PPA_URL <> show ver <>  "/HEADER.html"

fetchChangelog :: FilePath -> Version -> IO (Maybe Text)
fetchChangelog cacheDir ver = do
  bytes <- fetch cacheFile url
  -- TODO: parse changelog
  pure $ fmap (const $ fromString url) bytes
  where
    cacheFile = cacheDir </> "CHANGES"
    url = PPA_URL <> show ver <>  "/CHANGES"

fetchLastModified :: FilePath -> Version -> IO UTCTime
fetchLastModified cacheDir ver = cached cacheFile retrieve process
  where
    cacheFile = cacheDir </>"last-modified"
    url = PPA_URL <> show ver <> "/CHECKSUMS"

    retrieve = do
      lastModified <- httpLastModified' url >>= \case
        Nothing ->
          getCurrentTime
        Just utc ->
          pure utc
      pure $ Just . encodeUtf8 $ fromString (show lastModified)

    process = \case
      Just bytes ->
        pure . read . Text.unpack $ decodeUtf8 bytes
      Nothing ->
        error "assert: getCurrentTime is used for last_modified"

cached :: FilePath -> IO (Maybe ByteString) -> (Maybe ByteString -> IO a) -> IO a
cached file retrieve process = do
  hit <- doesFileExist file
  if hit then do
    traceM $ "Hit: " <> file
    bytes <- ByteString.readFile file
    process $ Just bytes
  else do
    traceM $ "Miss: " <> file
    mbytes <- retrieve
    for_ mbytes $
      ByteString.writeFile file
    process mbytes

fetch :: FilePath -> String -> IO (Maybe ByteString)
fetch file url = cached file retrieve pure
  where
    retrieve = do
      exists <- httpExists' url
      if exists then do
        res <- httpBS $ parseRequest_ url
        pure $ Just $ getResponseBody res
      else
        pure Nothing

-- listDebs :: Version -> IO [Text]
-- listDebs ver = do
--   let link = PPA_URL <> show ver <> "/"
--   content <- httpDirectory' link
--   let dirs = filter (Text.isSuffixOf "/") content
--   nestedContent <- traverse (httpDirectory' . (link +/+) . Text.unpack) dirs
--   pure $
--     map (Version.parseDeb (Text.pack link)) . filter required $
--       content <> fold nestedContent
--   where
--     required t = isAllDeb t || (isGeneric t && isAmd64Deb t)
--     isAllDeb = Text.isSuffixOf "_all.deb"
--     isAmd64Deb = Text.isSuffixOf "_amd64.deb"
--     isGeneric = Text.isInfixOf "generic"
