module Options
  ( Options(..)
  , parse
  ) where

import Data.Text qualified as Text
import Options.Applicative (Parser, execParser)
import Options.Applicative qualified as Opts

import Version (Version)
import Version qualified

data Options = Options
  { minVersion :: Version
  , fileName :: FilePath
  , noRC :: Bool
  , cacheDir :: FilePath
  }

parse :: IO Options
parse = execParser optsInfo
  where
    optsInfo =
      Opts.info (Opts.helper <*> optionsP) $
        Opts.fullDesc <>
        Opts.header "Feed-generator for https://kernel.ubuntu.com/~kernel-ppa/mainline/"

optionsP :: Parser Options
optionsP = do
  minVersion <- Opts.option (Opts.maybeReader $ Version.parse . Text.pack) $
    Opts.long "min-version"  <>
    Opts.help "Minimal kernel version. Format: x.yy[.zzz][-suffix]" <>
    maybe (error "blaze it") Opts.value (Version.parse "4.20")

  fileName <- Opts.strOption $
    Opts.long "output" <>
    Opts.short 'o' <>
    Opts.value "public/feed.xml" <>
    Opts.help "Name of output file. Default: feed.xml"

  noRC <- Opts.switch $
    Opts.long "no-rc" <>
    Opts.help "Delete versions with suffix from output."

  cacheDir <- Opts.strOption $
    Opts.long "cache" <>
    Opts.value ".cache" <>
    Opts.help "HTTP cache directory"

  pure Options{..}
